using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Abstraction;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.PromoCodes
{
    public class CardModel : CardModel<PromoCode>
    {
        private readonly IRepository<Preference> _preferenceRepository;

        public CardModel(IRepository<PromoCode> repository, IRepository<Preference> preferenceRepository)
            : base(repository)
        {
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<IActionResult> OnGetAsync(Guid? id)
        {
            ViewData["Preferences"] = new SelectList(await _preferenceRepository.GetAllAsync(), "Id", "Name");

            return await base.OnGetAsync(id);
        }
    }
}
