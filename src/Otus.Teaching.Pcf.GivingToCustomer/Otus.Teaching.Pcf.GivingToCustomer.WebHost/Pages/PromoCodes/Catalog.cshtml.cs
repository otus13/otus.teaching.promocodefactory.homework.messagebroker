using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Abstraction;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.PromoCodes
{
    public class CatalogModel : CatalogModel<PromoCode>
    {
        public CatalogModel(IRepository<PromoCode> repository) 
            : base(repository)
        {
        }
    }
}
