﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Abstraction
{
    public class CatalogModel<T> : PageModel
        where T : BaseEntity
    {
        protected readonly IRepository<T> Repository;
        public IEnumerable<T> Items { get; protected set; }

        public CatalogModel(IRepository<T> repository)
        {
            Repository = repository;
        }

        public virtual async Task<IActionResult> OnGetAsync()
        {
            Items = await Repository.GetAllAsync();
            return Page();
        }
    }
}
