﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Abstraction
{
    public class CardModel<T> : PageModel
        where T : BaseEntity
    {
        protected string CatalogPageName { get; set; } = "./Catalog";
        protected readonly IRepository<T> Repository;

        [BindProperty]
        public T Item { get; set; }
        public bool IsItemNew => Item.Id == Guid.Empty;

        public CardModel(IRepository<T> repository)
        {
            Repository = repository;
        }

        public virtual async Task<IActionResult> OnGetAsync(Guid? id)
        {
            if (id != null && id != Guid.Empty)
            {
                Item = await Repository.GetByIdAsync((Guid)id);
            }

            return Page();
        }

        public virtual async Task<IActionResult> OnPostSaveAsync()
        {
            if (IsItemNew)
            {
                await Repository.AddAsync(Item);

                return RedirectToPage(CatalogPageName);
            }

            await Repository.UpdateAsync(Item);

            return Page();
        }

        public virtual async Task<IActionResult> OnPostDeleteAsync()
        {
            if (!IsItemNew)
            {
                await Repository.DeleteAsync(Item);
            }

            return RedirectToPage(CatalogPageName);
        }
    }
}
