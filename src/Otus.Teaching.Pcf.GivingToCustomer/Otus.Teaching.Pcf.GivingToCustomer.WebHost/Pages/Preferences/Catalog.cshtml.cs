using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Abstraction;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Preferences
{
    public class CatalogModel : CatalogModel<Preference>
    {
        public CatalogModel(IRepository<Preference> repository) 
            : base(repository)
        {
        }
    }
}
