using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Abstraction;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Preferences
{
    public class CardModel : CardModel<Preference>
    {
        public CardModel(IRepository<Preference> repository)
            : base(repository)
        {
        }
    }
}
