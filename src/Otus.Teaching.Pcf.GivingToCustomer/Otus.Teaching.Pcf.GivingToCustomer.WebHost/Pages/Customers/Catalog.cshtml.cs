using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Abstraction;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Customers
{
    public class CatalogModel : CatalogModel<Customer>
    {
        public CatalogModel(IRepository<Customer> repository)
            : base(repository)
        {
        }
    }
}
