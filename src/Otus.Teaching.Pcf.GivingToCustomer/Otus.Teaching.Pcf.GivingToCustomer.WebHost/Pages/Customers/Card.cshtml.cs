using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Abstraction;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Customers
{
    public class CardModel : CardModel<Customer>
    {
        public CardModel(IRepository<Customer> repository)
            : base(repository)
        {
        }
    }
}
