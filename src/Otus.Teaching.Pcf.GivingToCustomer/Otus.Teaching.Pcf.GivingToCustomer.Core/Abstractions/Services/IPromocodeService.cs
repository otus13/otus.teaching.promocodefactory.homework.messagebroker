﻿using Otus.Teaching.Pcf.Events;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services
{
    public interface IPromocodeService
    {
        Task<bool> GivePromoCodesToCustomersWithPreferenceAsync(IReceivePromocode message);
    }
}
