﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class Customer
        :BaseEntity
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";
      
        [Required]
        public string Email { get; set; }

        public virtual ICollection<CustomerPreference> Preferences { get; set; }
        
        public virtual ICollection<PromoCodeCustomer> PromoCodes { get; set; }
    }
}